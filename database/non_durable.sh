#!/bin/bash

echo "Configuring postgres non-durable options."
echo "fsync = off" >> /var/lib/postgresql/data/postgresql.conf
echo "synchronous_commit = off" >> /var/lib/postgresql/data/postgresql.conf
echo "full_page_writes = off" >> /var/lib/postgresql/data/postgresql.conf
