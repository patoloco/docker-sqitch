-- Verify dummy:dummy_schema on pg

BEGIN;

SELECT pg_catalog.has_schema_privilege('dummy_schema', 'usage');

ROLLBACK;
