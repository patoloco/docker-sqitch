init:
	set -e
	createuser -SDr dummy
	createdb -O dummy dummy

dropdb:
	dropdb --if-exists dummy
	dropuser --if-exists dummy
